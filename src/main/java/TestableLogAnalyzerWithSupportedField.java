public class TestableLogAnalyzerWithSupportedField extends LogAnalyzerUsingFactoryMethodAndReturnResult {
    public boolean isSupported;
    protected boolean isValid(String fileName)
    {
        return isSupported;
    }
}