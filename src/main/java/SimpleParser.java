import com.sun.media.sound.InvalidFormatException;

import static java.lang.Integer.parseInt;

public class SimpleParser
{
    public int parseAndSum(String numbers) throws InvalidFormatException {
        if(numbers.length()==0)
        {
            return 0;
        }
        if(!numbers.contains(","))
        {
            return parseInt(numbers);
        }
        else
        {
            throw new InvalidFormatException("only 0 or 1");
        }
    }
}