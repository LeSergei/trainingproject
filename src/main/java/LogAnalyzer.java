import com.google.common.io.Files;

public class LogAnalyzer {

    private IExtensionManager manager;

    private static final String SLF_EXTENSION = "SLF";

    public LogAnalyzer() {
        manager = new FileExtensionManager();
    }

    public IExtensionManager getManager() {
        return manager;
    }

    public void setManager(IExtensionManager manager) {
        this.manager = manager;
    }

    boolean isValidLogFileName(String fileName)
    {
        String fileExtension = Files.getFileExtension(fileName);
        return SLF_EXTENSION.equals(fileExtension);
    }

    boolean isValidLogFileNameWithIgnoreCase(String fileName)
    {
        String file = Files.getFileExtension(fileName);
        return SLF_EXTENSION.equalsIgnoreCase(file);
    }




    public LogAnalyzer(IExtensionManager mgr)
    {
        manager = mgr;
    }

    public boolean isValidLogFileNameByManager(String fileName) {
        boolean result;
        try {
            result = manager.isValid(fileName);
        } catch (Exception e) {
            return false;
        }
        return result;
    }
}