import org.apache.commons.io.FilenameUtils;

public class LogAnalyzerUsingFactory {
    private IExtensionManager manager;

    public LogAnalyzerUsingFactory()
    {
        manager = new ExtensionManagerFactory().create();
    }

    public boolean IsValidLogFileName(String fileName)
    {
        try {
            return manager.isValid(fileName) && FilenameUtils.removeExtension(fileName).length()>5;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }
}