public interface IExtensionManager {
    //3.4.1. Выделение интерфейса с целью
    //подмены истинной реализации
    boolean isValid(String fileName) throws Exception;
}
