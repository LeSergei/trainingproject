//Определяем фабрику,которая умеет возвращать специальный диспетчер
public class ExtensionManagerFactory {
    private static IExtensionManager customManager = null;

    public IExtensionManager create() {
        if (customManager != null)
            return customManager;
        return new FileExtensionManager();
    }

    public static void setManager(IExtensionManager mgr) {
        customManager = mgr;
    }
}