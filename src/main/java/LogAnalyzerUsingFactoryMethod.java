public class LogAnalyzerUsingFactoryMethod {
    public boolean IsValidLogFileName(String fileName) throws Exception {
        return getManager().isValid(fileName);
    }
    protected IExtensionManager getManager()
    {
        return new FileExtensionManager();
    }
}