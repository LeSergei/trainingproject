import java.io.File;

public class FileExtensionManager implements IExtensionManager {
    public boolean isValid(String fileName)
    {
        return new File(fileName).exists();
    }
}