public class TestableLogAnalyzer extends LogAnalyzerUsingFactoryMethod {
    TestableLogAnalyzer(IExtensionManager mgr) {
        Manager = mgr;
    }

    private IExtensionManager Manager;

    @Override
    protected IExtensionManager getManager() {
        return Manager;
    }
}