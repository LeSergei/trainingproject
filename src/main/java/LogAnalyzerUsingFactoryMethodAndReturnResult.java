public class LogAnalyzerUsingFactoryMethodAndReturnResult {
    public boolean isValidLogFileName(String fileName) throws Exception {
        return this.isValid(fileName);
    }

    protected boolean isValid(String fileName)
    {
        FileExtensionManager mgr = new FileExtensionManager();
        return mgr.isValid(fileName);
    }
}