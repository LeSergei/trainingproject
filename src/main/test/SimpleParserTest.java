import com.sun.media.sound.InvalidFormatException;
import org.testng.Assert;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

import static java.lang.Integer.parseInt;

public class SimpleParserTest {

    @Test(description = "", expectedExceptions = InvalidFormatException.class, expectedExceptionsMessageRegExp = "only 0 or 1")
    public void ParseAndSumThrowExceptionTest() throws InvalidFormatException {
       // System.out.println("only 0 or 1");
        new SimpleParser().parseAndSum("10,1");

    }

    @Test(description = "")
    public void ParseAndSumZeroLengthTest() throws InvalidFormatException {

        Assert.assertEquals(new SimpleParser().parseAndSum(""), 0);
       // System.out.println("string is empty");
    }

    @Test(description = "")
    public void ParseAndSumStringIsIntValueTest() throws InvalidFormatException {
        String a = "6";
        Assert.assertEquals(new SimpleParser().parseAndSum(a), parseInt(a));
        //System.out.println("string is Int Value " + a);
    }

    @AfterMethod
    public void afterMethod(ITestResult result) {
        System.out.println("method name: " + result.getMethod().getMethodName() + " status: " + result.getStatus());
    }
}