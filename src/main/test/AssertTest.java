import com.sun.media.sound.InvalidFormatException;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AssertTest {
    @Test(description = "2.4.1. Класс Assert: assertEquals")
    public void assertEqualsTest() {
        Assert.assertEquals(2, 1+1, "1 + 1 = 2");
    }

    @Test(description = "2.4.1. Класс Assert: assertSame")
    public void assertSameTest() {
        Assert.assertSame(Integer.parseInt("1"), Integer.parseInt("1"), "Этот тест должен пройти");
    }
}