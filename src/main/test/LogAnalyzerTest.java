import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;


public class LogAnalyzerTest {
    @Test(description = "2.4 Создание первого теста" +
            "1. Подготовка (Arrange) объектов, то есть создание и настройка.\n" +
            "2. Воздействие (Act) на объект.\n" +
            "3. Утверждение (Assert) об ожидаемом результате.")
    public void isValidLogFileNameTest() {
        LogAnalyzer logAnalyzer = new LogAnalyzer(); // 1. Подготовка (Arrange) объектов, то есть создание и настройка."
        boolean resultTrue = logAnalyzer.isValidLogFileName("test.SLF"); //"2. Воздействие (Act) на объект."
        Assert.assertTrue(resultTrue); //3. Утверждение (Assert) об ожидаемом результате."
        boolean resultFalse = logAnalyzer.isValidLogFileName("test.txt"); //"2. Воздействие (Act) на объект."
        Assert.assertFalse(resultFalse); //3. Утверждение (Assert) об ожидаемом результате."
    }

    @Test(description = "2.4.3 Добавление положительных тестов")
    public void isValidLogFileNameWithIgnoreCaseTest() {
        LogAnalyzer logAnalyzer = new LogAnalyzer();

        boolean resultFalse = logAnalyzer.isValidLogFileName("test.slf");
        Assert.assertFalse(resultFalse);

        boolean resultIgnoreCaseTrue = logAnalyzer.isValidLogFileNameWithIgnoreCase("test.slf");
        Assert.assertTrue(resultIgnoreCaseTrue);
    }

    @DataProvider(name = "isValidLogFileNameParameterizedDataProvider")
    public static Object[][] primeNumbers() {
        return new Object[][]{
                {"test.slf", true},
                {"test.txt", false},
                {"test.SLF", true},
                {"test.TXT", false},
                {"test", false}
        };
    }

    @Test(description = "2.5 Рефакторинг – параметризованные тесты",
            dataProvider = "isValidLogFileNameParameterizedDataProvider")
    public void isValidLogFileNameParameterizedTest(String fileName, boolean expectedResult) {
        LogAnalyzer logAnalyzer = new LogAnalyzer();
        boolean result = logAnalyzer.isValidLogFileNameWithIgnoreCase(fileName);
        Assert.assertEquals(result, expectedResult);
    }


    @Test(description = "3.4.3. Внедрение подделки на уровне конструктора (внедрение через конструктор)")
    public void isValidFileName_NameSupportedExtension_ReturnsTrue() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.WillBeValid = true;
        LogAnalyzer log = new LogAnalyzer(myFakeManager);
        boolean result = log.isValidLogFileNameByManager("short.ext");
        Assert.assertTrue(result);
    }

    //Определяем заглушку самым простым способом
    class FakeExtensionManager implements IExtensionManager {
        boolean WillBeValid = false;

        public boolean isValid(String fileName) {
            return WillBeValid;
        }
    }

    @Test(description = "3.4.4. Имитация исключений от подделок")
    public void isValidFileName_ExtManagerThrowsException_ReturnsFalse() {
        FakeExtensionManagerForException myFakeManager = new FakeExtensionManagerForException();
        myFakeManager.willThrow = new Exception("Это подделка");
        LogAnalyzer log = new LogAnalyzer(myFakeManager);
        boolean result = log.isValidLogFileNameByManager("anything.anyextension");
        Assert.assertFalse(result);
    }

    //Определяем заглушку самым простым способом
    class FakeExtensionManagerForException implements IExtensionManager {
        boolean willBeValid = false;
        Exception willThrow = null;

        public boolean isValid(String fileName) throws Exception {
            if (willThrow != null) {
                throw willThrow;
            }
            return willBeValid;
        }
    }

    @Test(description = "3.4.5. Внедрение подделки через установку свойства")
    public void IsValidFileName_SupportedExtension_ReturnsTrue() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.WillBeValid = true;
        LogAnalyzer log = new LogAnalyzer(myFakeManager);
        log.setManager(myFakeManager);
        boolean result = log.isValidLogFileNameByManager("anything.anyextension");
        Assert.assertTrue(result);
    }

    @Test(description = "3.4.6. Внедрение подделки непосредственно перед вызовом метода")
    public void IsValidFileName_SupportedExtension_ReturnsTrue_UseFactory() {
        FakeExtensionManager myFakeManager = new FakeExtensionManager();
        myFakeManager.WillBeValid = true;

        ExtensionManagerFactory.setManager(myFakeManager);

        LogAnalyzerUsingFactory log = new LogAnalyzerUsingFactory();
        boolean result = log.IsValidLogFileName("anything.anyextension");
        Assert.assertTrue(result);
    }

    @Test(description = "3.4.6.Поддельный метод – использование локального фабричного метода" +
            " (выделить и переопределить)")
    public void overrideTest() throws Exception {
        FakeExtensionManager stub = new FakeExtensionManager();
        stub.WillBeValid = true;

        TestableLogAnalyzer logan = new TestableLogAnalyzer(stub);

        boolean result = logan.IsValidLogFileName("file.ext");
        Assert.assertTrue(result);
    }

    @Test(description = "3.5.1. Использование выделения\n" +
            "и переопределения для создания\n" +
            "поддельных результатов")
    public void overrideTestWithoutStub() throws Exception {
        TestableLogAnalyzerWithSupportedField logan = new TestableLogAnalyzerWithSupportedField();
        logan.isSupported = true;

        boolean result = logan.isValidLogFileName("file.ext");
        Assert.assertTrue(result, "...");
    }
}